# Generated by Django 4.0.3 on 2022-11-17 22:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accountvo',
            name='import_href',
        ),
        migrations.AlterField(
            model_name='accountvo',
            name='is_active',
            field=models.BooleanField(null=True),
        ),
    ]
