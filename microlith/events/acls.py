from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}"
    headers = {"authorization": PEXELS_API_KEY}
    resp = requests.get(url, headers=headers)
    data = resp.json()

    return data["photos"][0]["src"]["original"]


def get_coord(city, state):

    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=&appid={OPEN_WEATHER_API_KEY}"
    resp = requests.get(url)
    return {"lat": resp.json()[0]["lat"], "lon": resp.json()[0]["lon"]}


def get_weather_data(lat, lon):

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    resp = requests.get(url)
    return {
        "description": resp.json()["weather"][0]["description"],
        "temp": resp.json()["main"]["temp"],
    }
