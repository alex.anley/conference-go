from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation, Status
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
from events.api_views import ConferenceListEncoder
import pika


"""ENCODERS"""


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "status",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}


class ListPresentationEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}


"""VIEW FUNCTIONS"""


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentation = (
            Presentation.objects.all()
        )  # or filter by (id=conference_id)
        return JsonResponse(
            presentation,
            encoder=ListPresentationEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Ivalid Conference id"},
            )
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=ListPresentationEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})  # this is a Boolean
    else:
        presentation = Presentation.objects.get(id=id)
        content = json.loads(request.body)  # this turns content into a dict
        # new code:
        if content.get("status"):
            status = Status.objects.get(name=content["status"])
            content["status"] = status.id

    Presentation.objects.filter(id=id).update(**content)
    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        presentation, encoder=PresentationDetailEncoder, safe=False
    )


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_approvals",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        ),
    )
    connection.close()
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject()
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="presentation_rejections")
    channel.basic_publish(
        exchange="",
        routing_key="presentation_rejections",
        body=json.dumps(
            {
                "presenter_name": presentation.presenter_name,
                "presenter_email": presentation.presenter_email,
                "title": presentation.title,
            }
        ),
    )
    connection.close()
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


# Warren's code:

# def send_message(queue_name, body):
#     parameters = pika.ConnectionParameters(host="rabbitmq")
#     connection = pika.BlockingConnection(parameters)
#     channel = connection.channel()
#     channel.queue_declare(queue=queue_name)
#     channel.basic_publish(
#         exchange="",
#         routing_key=queue_name,
#         body=json.dumps(body),
#     )
#     connection.close()


# @require_http_methods(["PUT"])
# def api_approve_presentation(request, pk):
#     presentation = Presentation.objects.get(id=pk)
#     presentation.approve()
#     body = {
#         "presenter_name": presentation.presenter_name,
#         "presenter_email": presentation.presenter_email,
#         "title": presentation.title,
#     }
#     send_message("presentation_approvals", body)
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )


# @require_http_methods(["PUT"])
# def api_reject_presentation(request, pk):
#     presentation = Presentation.objects.get(id=pk)
#     presentation.reject()
#     body = {
#         "presenter_name": presentation.presenter_name,
#         "presenter_email": presentation.presenter_email,
#         "title": presentation.title,
#     }
#     send_message("presentation_rejections", body)
#     return JsonResponse(
#         presentation,
#         encoder=PresentationDetailEncoder,
#         safe=False,
#     )
